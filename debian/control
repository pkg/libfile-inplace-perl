Source: libfile-inplace-perl
Section: perl
Testsuite: autopkgtest-pkg-perl
Rules-Requires-Root: no
Priority: optional
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Salvatore Bonaccorso <carnil@debian.org>
Build-Depends: debhelper (>= 10)
Build-Depends-Indep: perl
Standards-Version: 4.1.3
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-perl/packages/libfile-inplace-perl.git
Vcs-Git: https://anonscm.debian.org/git/pkg-perl/packages/libfile-inplace-perl.git
Homepage: https://metacpan.org/release/File-Inplace

Package: libfile-inplace-perl
Architecture: all
Depends: ${misc:Depends}, ${perl:Depends}
Description: Perl module to ease editing a file in-place
 File::Inplace is a perl module intended to ease the common task of editing a
 file in-place. Inspired by variations of perl's -i option, this module is
 intended for somewhat more structured and reusable editing than command line
 perl typically allows. File::Inplace endeavors to guarantee file integrity;
 that is, either all of the changes made will be saved to the file, or none
 will. It also offers functionality such as backup creation, automatic field
 splitting per-line, automatic chomping/unchomping, and aborting edits
 partially through without affecting the original file.
